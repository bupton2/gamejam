<?php
//error_reporting(0);
final class Player {
	private $inventory;
	private $hunger;
	private $life;
	private $location;
	private $displayText;
	
	public function __construct() {
		for ($q = 1; $q <= 10; $q++){
			$inventory = array("$q. "=>"None");
		}
		$hunger = 100;
		$life = 100;
		$location = "lobby";
		$displayText = NULL;
		$key1 = rand(100,999);
		$key2 = rand(100,999);
		$key3 = rand(100,999);
		$finishKey = $key1 . $key2 . $key3;
	}
	
		
	public function addInventory($item){
		array_unshift($inventory,$item);
	}
	
	public function dropInventory($invKey){
		//removes the item from the inventory array specified by the key passed by the parameter
		$removeInventory=array($invKey=>"Empty");
		array_splice($this->$inventory,0,1,$removeInventory);
	}
	
	public function displayInventory(){
		foreach($inventory as $index => $item) {
		
		echo <<< EOS
		   <div class="col-xs-6 col-xs-6 col-md-6 col-lg-6"><p id=firstaid class="item">$index : $item</p></div>
EOS;
		}
	}
	
	public function addRoom(){
		$i = rand(1,10);
		$filename = "./rooms/";
		
		switch ($i) {
			case 1:
			case 2:
			$filename .= "parlor.html";
			break;
			
			case 3:
			case 4:
			$filename .= "boiler.html";
			break;
			
			case 5:
			case 6:
			$filename .= "attic.html";
			break;
			
			case 7:
			case 8:
			$filename .= "pool.html";
			break;
			
			case 9:
			case 10:
			$filename .= "filthypool.html";
			break;
			
		$displayText = file_get_contents($filename, FILE_USE_INCLUDE_PATH);
		return $displayText;
		}
	}
	
	public function addStuff(){
		$j = rand(1,10);
		$filename = "./items/";
		
		switch ($i) {
			
			case 1:
			$filename .= "wilma.html";
			break;

			case 2:
			$filename .= "chest.html";
			break;
			
			case 3:
			case 4:
			$filename .= "box.html";
			break;
			
			case 5:
			case 6:
			$filename .= "locker.html";
			break;
			
			case 7:
			case 8:
			$filename .= "pool.html";
			break;
			
			case 9:
			case 10:
			$filename .= "filthypool.html";
			break;
									
		$displayText = file_get_contents($filename, FILE_USE_INCLUDE_PATH);
		return $displayText;

		}
	}
	
	public function addWilma(){
		
	}
	
	public function addFred(){
		
	}
	
	public function addBetty(){
		
	}
	
	public function addBamBam(){
		
	}
	
	public function heal($heal){
		if(is_numeric($heal)){
		$life = $life + $heal;
			if($life < 1){
				killPlayer();
			}else if($life > 100){
				$life = 100;
			}
		}
		return $life;
	}
	
	public function getLife(){
		return $life;
	}
	
	public function getHunger(){
		return $hunger;
	}
	
	public function feed($food){
		if(is_numeric($food)){
			$hunger += $food;
			if($hunger < 1){
				$life--;
				$displayText = "soooo hungry";
			}else if($hunger > 100){
				$hunger = 100;
			}
		}
		return $hunger;
	}
	
	public function killPlayer(){
		$life = 0;
		$hunger = 0;
		$filename = "./other/death.html";
		$displayText = file_get_contents($filename, FILE_USE_INCLUDE_PATH);
		return $displayText;
	}
	
	public function checkWin($check){
		$filename = "./other/";
		if($check == $finishKey){
			$filename .= "solved.html";
			$displayText = file_get_contents($filename, FILE_USE_INCLUDE_PATH);
			return $displayText;
		}else{
			$filename .= "tricked.html";
			$displayText = file_get_contents($filename, FILE_USE_INCLUDE_PATH);
		return $displayText;
		}
	}
}
?>
